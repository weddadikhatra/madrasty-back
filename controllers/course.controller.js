const express = require('express')
const router = express.Router()
const Course = require('../models/course.model')
const { generateCrudMethods,generateExtraMethods } = require('../services')
const courseCrud = generateCrudMethods(Course)
const extraCourseMethods = generateExtraMethods(Course);
const { raiseRecord404Error } = require('../middlewares');


router.get('/', (req, res, next) => {
    courseCrud.getAll()
        .then(data => res.send(data))
        .catch(err => next(err))
})

router.get('getbyid/:id', (req, res, next) => {
    courseCrud.getById(req.params.id)
        .then(data => {
            if (data) res.send(data)
            else raiseRecord404Error(req, res)
        })
        .catch(err => next(err))
})

router.post('/', (req, res, next) => {
    courseCrud.create(req.body)
        .then(data => res.status(201).json(data))
        .catch(err => next(err))
})

router.put('updatebyid/:id', (req, res) => {
    courseCrud.update(req.params.id, req.body)
        .then(data => {
            if (data) res.send(data)
            else raiseRecord404Error(req, res)
        })
        .catch(err => next(err))
})

router.delete('getbyid/:id', (req, res) => {
    courseCrud.delete(req.params.id)
        .then(data => {
            if (data) res.send(data)
            else raiseRecord404Error(req, res)
        })
        .catch(err => next(err))
})

router.get('/category/:category', (req, res, next) => {
    const category = req.params.category; 
    const cat=category.replace("-"," ")

    extraCourseMethods.findByCategory(cat)
        .then(data => res.send(data))
        .catch(err => next(err))
})

router.get('/3lastcourses', (req, res, next) => {

    Course.find()
          .sort({ createdAt: -1 }) 
          .limit(3)
          .then(data => res.send(data))
          .catch(err => next(err))
})



module.exports = router