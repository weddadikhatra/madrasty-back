const express = require('express');
const router = express.Router();
const Course = require('../models/course.model'); // Replace with your Course model path
const PurchasedCrs = require('../models/purchase.model');
const User = require('../models/users.model');

router.get('/', async (req, res, next) => {
    try {
        const courseCount = await Course.countDocuments();
        const purchasedCrsCount = await PurchasedCrs.countDocuments();
        const userCount = await User.countDocuments();

        res.json({
            nbOfCourses: courseCount,
            nbOfPurchasedCrs: purchasedCrsCount,
            nbOfStudents: userCount
        });
    } catch (err) {
        next(err); // Pass errors to your error handler middleware
    }
});

module.exports = router;
