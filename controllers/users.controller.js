const express = require('express')
const router = express.Router()
const Employee = require('../models/users.model')
const { generateCrudMethods } = require('../services')
const employeeCrud = generateCrudMethods(Employee)
const { validateDbId, raiseRecord404Error } = require('../middlewares');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.get('/', (req, res, next) => {
    employeeCrud.getAll()
        .then(data => res.send(data))
        .catch(err => next(err))
})

router.get('/:id', validateDbId, (req, res, next) => {
    employeeCrud.getById(req.params.id)
        .then(data => {
            if (data) res.send(data)
            else raiseRecord404Error(req, res)
        })
        .catch(err => next(err))
})

router.post('/', (req, res, next) => {
    employeeCrud.create(req.body)
        .then(data => res.status(201).json(data))
        .catch(err => next(err))
})

router.put('/:id', validateDbId, (req, res) => {
    employeeCrud.update(req.params.id, req.body)
        .then(data => {
            if (data) res.send(data)
            else raiseRecord404Error(req, res)
        })
        .catch(err => next(err))
})

router.delete('/:id', validateDbId, (req, res) => {
    employeeCrud.delete(req.params.id)
        .then(data => {
            if (data) res.send(data)
            else raiseRecord404Error(req, res)
        })
        .catch(err => next(err))
})

router.post('/login', async (req, res, next) => {
    const { Email, Pwd } = req.body;

    try {
        // 1. Find the user by email
        const user = await Employee.findOne({ Email });
        if (!user) {
            return res.status(401).json({ error: 'Invalid email or password' });
        }

        if (Pwd!=user.Pwd) {
            return res.status(401).json({ error: 'Invalid email or password '+Pwd+"--- "+user.Pwd });
        }

        return res.status(200).json(user)

    } catch (err) {
        next(err);
    }
});


module.exports = router