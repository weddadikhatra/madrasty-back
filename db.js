const mongoose = require('mongoose')

const dbUri = 'mongodb+srv://weddadi:weddadi449@cluster0.p7ffsaf.mongodb.net/madrasty?retryWrites=true&w=majority&appName=Cluster0'

mongoose.set('strictQuery', false)

module.exports = () => {
    return mongoose.connect(dbUri,
        { useNewUrlParser: true, useUnifiedTopology: true })
}