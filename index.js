const express = require('express')
const bodyParser = require('body-parser')

const connectDb = require('./db.js')
const UserRoutes = require('./controllers/users.controller.js')
const CourseRoutes = require('./controllers/course.controller.js')
const PurchasesRoutes = require('./controllers/purchases.controller.js')
const StatistiqueRoutes = require('./controllers/statistique.controller.js')

const { errorHandler } = require('./middlewares')


const app = express()
app.use(bodyParser.json())
app.use('/api/users', UserRoutes)
app.use('/api/courses', CourseRoutes)
app.use('/api/purchases', PurchasesRoutes)
app.use('/api/statistique', StatistiqueRoutes)
app.use(errorHandler)


connectDb()
    .then(() => {
        console.log('db connection succeeded.')
        app.listen(3000,
            () => console.log('server started at 3000.'))
    })
    .catch(err => console.log(err))