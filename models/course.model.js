const mongoose = require('mongoose')

module.exports = mongoose.model('Course', {
    Nom: { type: String },
    Categorie: { type: String },
    Description: { type: String },
    image: { type: String },
    price: { type: Number },
    video: { type: String },
    createdAt: { type: Date, default: Date.now } 

})