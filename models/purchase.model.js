const mongoose = require('mongoose')
const Schema = mongoose.Schema; // Helps with references

module.exports = mongoose.model('Purchase', {
    Date: { type: String },
    Course: { type: Schema.Types.ObjectId, ref: 'Course' },
    Description: { type: String },
    Amount: { type: Number },
    Method: { type: String },
    User:{ type: Schema.Types.ObjectId, ref: 'User' }
})





