const mongoose = require('mongoose')

module.exports = mongoose.model('User', {
    Nom: { type: String },
    Email: { type: String },
    Pwd: { type: String },
    Role: { type: Number },
})